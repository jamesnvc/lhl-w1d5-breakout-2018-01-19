//
//  Doctor.m
//  Breakout
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Doctor.h"

static NSMutableArray * prescriptions;

static int numDocs = 0;

@implementation Doctor

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    numDocs += 1;
    NSLog(@"Creating doctor #%d", numDocs);
    return [super allocWithZone:zone];
}

- (void)addPrescription:(NSString*)script forPatient:(id)patient
{
    // do whatever stuff
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        prescriptions = [[NSMutableArray alloc] init];
    });
    [prescriptions addObject:script];
}

- (int)getValue
{
    static int n = 0;
    n += 1;
    return n;
}

@end
