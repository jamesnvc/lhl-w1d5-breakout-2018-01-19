//
//  Foobar.h
//  Breakout
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

//enum Thing { foo, bar, baz };
typedef enum { foo, bar, baz } Thing;
typedef NSInteger Whatever;

typedef NS_ENUM(NSUInteger, PrimaryColour) {
    PrimaryColourRed,
    PrimaryColourBlue,
    PrimaryColourGreen,
};

// enum { pcR, pcB, pcG } PColour;

@interface Foobar : NSObject

//@property (nonatomic,assign) enum Thing whatever;
@property (nonatomic,assign) Thing whatever;
@property (nonatomic,assign) Whatever thingy;
@property (nonatomic,assign) enum PrimaryColour colour;


@end

//Whatever aeou = 1 + 2;
