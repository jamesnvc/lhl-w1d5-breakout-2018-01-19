//
//  main.m
//  Breakout
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "Foobar.h"
#include "Doctor.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...

        Doctor *d1 = [Doctor new];
        NSLog(@"Value = %d",[d1 getValue]);
        NSLog(@"Value = %d",[d1 getValue]);
        Doctor *d2 = [Doctor new];
        NSLog(@"Value = %d",[d2 getValue]);
        NSLog(@"Value = %d",[d2 getValue]);
//        [d2 performSelector:@selector(foobar:) withObject:nil];

        NSString *s = @[ @(1) ][0];
        NSLog(@"%@", [s uppercaseString]);

    }
    return 0;
}
