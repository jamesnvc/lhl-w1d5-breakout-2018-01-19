//
//  Doctor.h
//  Breakout
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Doctor : NSObject
- (int)getValue;
@end
